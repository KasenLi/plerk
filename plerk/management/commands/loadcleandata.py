import numpy as np
import pandas as pd
from django.core.management.base import BaseCommand
from plerk.settings import BASE_DIR


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        """
            Read test_database.csv and clean the data

            Creates transactions.csv and companies.csv with the respective information
        """
        self.stdout.write(self.style.NOTICE('Starting process'))
        try:
            data = pd.read_csv(f'{BASE_DIR}/static/test_database.csv')
        except Exception:
            self.stdout.write(self.style.ERROR(
                'File test_database.csv not found'))
            return

        original_company = data['company'].str.title()
        company = data['company'].str.lower().str.replace(' ', '')

        df_company = pd.DataFrame(company)
        df_company.insert(1, 'original', original_company)
        df_company.drop_duplicates(subset=['company'], inplace=True)
        df_company = df_company.dropna()
        df_company.insert(0, 'company_id', range(1, 1 + len(df_company)))
        data['company'] = data['company'].str.lower().str.replace(' ', '')
        data = data.dropna()

        merge_data = pd.merge(data, df_company, on="company", how="inner")

        df_company["status"] = True

        merge_data["status_transaction"] = merge_data["status_transaction"].str.upper()
        merge_data.drop('company', axis=1, inplace=True)
        merge_data.drop('original', axis=1, inplace=True)
        merge_data['final_charge'] = np.where(
            (merge_data['status_transaction'] == 'CLOSED') &
            (merge_data['status_approved'] == True),
            True, False)
        merge_data.to_csv(
            index=False, path_or_buf=f'{BASE_DIR}/static/transactions.csv')
        df_company.to_csv(
            index=False, path_or_buf=f'{BASE_DIR}/static/companies.csv')
        self.stdout.write(self.style.SUCCESS(
            f'Loaded {len(df_company)} companies and {len(merge_data)} transactions'))
