from datetime import datetime
from rest_framework import status
from admin.models import Token

def validate_token(api_token):
    """
        Method that validates the API Token

        Arguments:
        - api_token: API Token

        Return True if the token is found
        Return dict with error and status if something is wrong
    """
    if api_token is None:
        return {
            'data': { 'Error': 'Missing authorization token' },
            'status': status.HTTP_400_BAD_REQUEST
        }
    try:
        token = Token.objects.get(token=api_token, status='AVAILABLE')
        valid_before = datetime.fromisoformat(str(token.valid_before)).timestamp()
        if datetime.now().timestamp() > valid_before:
            token.expired_at = datetime.now()
            token.status = 'EXPIRED'
            return {
                'data': { 'Error': 'Token expired' },
                'status': status.HTTP_401_UNAUTHORIZED
            }
        return True
    except Token.DoesNotExist:
        return {
            'data': { 'Error': 'Invalid token' },
            'status': status.HTTP_401_UNAUTHORIZED
        }
