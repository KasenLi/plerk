"""plerk URL Configuration
"""
from django.urls import path
from admin import views as admin
from companies import views as company
from transactions import views as transaction

urlpatterns = [
    path('user/getToken', admin.Admin.as_view()),
    path('company/<int:id>', company.CompanySummary.as_view()),
    path('transactions/summary', transaction.TransactionsSummary.as_view()),
    path('transactions/pending', transaction.PendingTransactions.as_view())
]
