# Plerk

## Environment configuration with venv
Note: Use the virtual environment of your preference
```sh
$ python3.8 -m venv .venv             # Will create a folder with the environment
$ source .venv/bin/activate           # Enables your environment
$ pip install -U pip                  # Update pip
$ pip install -r requirements.txt     # Install the dependencies
$ deactivate                          # Closes your environment
```

## Installation
```sh
$ python manage.py loadcleandata      # Clean data and load into new CSV files
$ python manage.py migrate            # Run migrations
$ python manage.py loaduser           # Load initial testing user
```

## Run server
```sh
$ python manage.py runserver
```

## Available endpoints

[POST] /user/getToken

Body
```json
{
  "username": "username",
  "password": "password"
}
```

**The next endpoints require Authorization Header with a valid token that can be extracted from /user/getToken**

[GET] /transactions/summary

[GET] /company/:id

[GET] /transactions/pending