
from django.db import connection
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from companies.models import Company
from transactions.models import Transaction

from plerk.utils import validate_token


class TransactionsSummary(APIView):
    """
      View for transactions summary
    """

    def most_sales(self):
        """
            Method that calculate the company with most sales

            Return company model object
        """

        with connection.cursor() as cursor:
            cursor.execute("""
                WITH companies_sales AS(SELECT transac.company_id,
                COUNT(transac.id) FILTER (WHERE final_charge = true) AS sales
                FROM transactions_transaction transac
                GROUP BY transac.company_id) 
                SELECT cs.company_id, max(cs.sales) AS c_sales FROM companies_sales cs
                GROUP BY cs.company_id ORDER BY c_sales DESC
            """)
            companies_sales = cursor.fetchall()

            company_with_most_sales = companies_sales[0]
            try:
                company = Company.objects.get(pk=company_with_most_sales[0],
                                            is_active=True, is_deleted=False)
            except Company.DoesNotExist:
                return None

            return company

    def less_sales(self):
        """
            Method that calculate the company with less sales

            Return company model object
        """

        with connection.cursor() as cursor:
            cursor.execute("""
                WITH companies_sales AS(SELECT transac.company_id,
                COUNT(transac.id) FILTER (WHERE final_charge = true) AS sales
                FROM transactions_transaction transac
                GROUP BY transac.company_id) 
                SELECT cs.company_id, max(cs.sales) AS c_sales FROM companies_sales cs
                GROUP BY cs.company_id ORDER BY c_sales ASC
            """)
            companies_sales = cursor.fetchall()

            company_with_less_sales = companies_sales[0]
            try:
                company = Company.objects.get(pk=company_with_less_sales[0],
                                            is_active=True, is_deleted=False)
            except Company.DoesNotExist:
                return None

            return company

    def total_price_completed_transactions(self):
        """
            Method that calculate the total amount
            of completed transactions

            Return (float) price
        """

        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT SUM(price) FILTER (WHERE final_charge = true) AS total
                FROM transactions_transaction
                WHERE is_deleted = False
            """)
            total = cursor.fetchall()

            return total[0][0]

    def total_price_uncompleted_transactions(self):
        """
            Method that calculate the total amount
            of uncompleted transactions

            Return (float) price
        """

        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT SUM(price) FILTER (WHERE final_charge = false) AS total
                FROM transactions_transaction
                WHERE is_deleted = False
            """)
            total = cursor.fetchall()

            return total[0][0]

    def most_rejected(self):
        """
            Method that calculate the company with most
            failed transactions

            Return company model object
        """

        with connection.cursor() as cursor:
            cursor.execute("""
                WITH companies_sales AS(SELECT transac.company_id,
                COUNT(transac.id) FILTER (where final_charge = false) AS sales
                FROM transactions_transaction transac
                GROUP BY transac.company_id) 
                SELECT cs.company_id, max(cs.sales) AS c_sales FROM companies_sales cs
                GROUP BY cs.company_id ORDER BY c_sales DESC
            """)
            companies_sales = cursor.fetchall()

            company_with_most_failed_transactions = companies_sales[0]
            try:
                company = Company.objects.get(
                    pk=company_with_most_failed_transactions[0],
                    is_active=True, is_deleted=False)
            except Company.DoesNotExist:
                return None

            return company

    def get(self, request):
        """
            Calculate and show transactions summary

            Return a summary of transactions
        """

        """ API Authentication """
        api_token = request.META.get('HTTP_AUTHORIZATION', None)
        is_valid = validate_token(api_token)
        if type(is_valid) is dict:
            return Response(is_valid['data'], status=is_valid['status'])
        """ API Authentication """

        response = {
            'company_with_most_sales': self.most_sales().name,
            'company_with_less_sales': self.less_sales().name,
            'total_price_completed_transactions': self.total_price_completed_transactions(),
            'total_price_uncompleted_transactions': self.total_price_uncompleted_transactions(),
            'company_with_most_failed_transactions': self.most_rejected().name
        }

        return Response(response, status=status.HTTP_200_OK)


class PendingTransactions(APIView):
    """
      View for pending transactions
    """

    def get(self, request):
        """
            Get transactions with transactions_status = PENDING

            Return pending transactions
        """
        """ API Authentication """
        api_token = request.META.get('HTTP_AUTHORIZATION', None)
        is_valid = validate_token(api_token)
        if type(is_valid) is dict:
            return Response(is_valid['data'], status=is_valid['status'])
        """ API Authentication """

        transactions = Transaction.objects.prefetch_related('company')\
            .filter(transaction_status='PENDING', is_deleted=False)

        result = []
        for transaction in transactions:
            result.append({
                'id': transaction.id,
                'price': transaction.price,
                'date': transaction.date,
                'company': {
                    'id': transaction.company.id,
                    'name': transaction.company.name
                }
            })

        return Response(result, status=status.HTTP_200_OK)
