""" Transactions model """
from django.db import models


class Transaction(models.Model):
    """ Transactions """
    id = models.BigAutoField(primary_key=True)

    price = models.FloatField(null=False, max_length=255)
    date = models.DateTimeField()

    transaction_status = models.CharField(max_length=12, null=False, choices=(
        ('CLOSED', 'Closed'),
        ('PENDING', 'Pending'),
        ('REVERSED', 'Reversed'),
        ('FUNDING', 'Funding'),
        ('FUNDING-USER', 'Funding user')
    ))

    status_approved = models.BooleanField(
        null=False, default=False, db_index=True)
    company = models.ForeignKey(
        'companies.Company', on_delete=models.CASCADE, null=False,
        db_index=True)
    final_charge = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False, db_index=True)
    deleted_at = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
