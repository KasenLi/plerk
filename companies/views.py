
from django.db import connection
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from companies.models import Company

from plerk.utils import validate_token


class CompanySummary(APIView):
    """
      View to company transactions summary
    """

    def total_price_completed_transactions(self, id):
        """
            Method that calculates the total amount
            of completed transactions

            Arguments:
            - id: ID of the company

            Return fecth with total and count
        """

        with connection.cursor() as cursor:
            cursor.execute(f"""
                SELECT SUM(price) FILTER (WHERE final_charge = true) AS total,
                COUNT(transac.id) FILTER (WHERE final_charge = true)
                FROM transactions_transaction AS transac
		        WHERE transac.company_id = {id} AND transac.is_deleted = false
            """)
            total = cursor.fetchall()

            return total[0]

    def total_price_uncompleted_transactions(self, id):
        """
            Method that calculates the total amount
            of uncompleted transactions

            Arguments:
            - id: ID of the company

            Return fecth with total and count
        """

        with connection.cursor() as cursor:
            cursor.execute(f"""
                SELECT SUM(price) FILTER (WHERE final_charge = false) AS total,
                COUNT(transac.id) FILTER (WHERE final_charge = false)
                FROM transactions_transaction AS transac
		        WHERE transac.company_id = {id} AND transac.is_deleted = false
            """)
            total = cursor.fetchall()

            return total[0]

    def date_with_most_transactions(self, id):
        """
            Method that calculates the date with most transactions

            Arguments:
            - id: ID of the company

            Return date
        """

        with connection.cursor() as cursor:
            cursor.execute(f"""
                WITH companies_sales AS(SELECT to_char(transac.date, 'YYYY-MM-DD') as t_date,
                COUNT(transac.id) AS sales
                FROM transactions_transaction transac
                WHERE transac.company_id = {id}
                GROUP BY t_date) 
                SELECT cs.t_date, max(cs.sales) AS c_sales FROM companies_sales cs
                GROUP BY cs.t_date ORDER BY c_sales DESC
            """)
            companies_sales = cursor.fetchall()

            date = companies_sales[0]

            return date[0]

    def get(self, request, id):
        """
            Calculate and show company transactions information

            Arguments:
            - id: ID of the company

            Return a summary of transactions
        """
        """ API Authentication """
        api_token = request.META.get('HTTP_AUTHORIZATION', None)
        is_valid = validate_token(api_token)
        if type(is_valid) is dict:
            return Response(is_valid['data'], status=is_valid['status'])
        """ API Authentication """

        try:
            company = Company.objects.get(pk=id, is_active=True, is_deleted=False)
        except Company.DoesNotExist:
            return Response(
                {
                    'Error': 'Company is not active or not found'
                },
                status=status.HTTP_404_NOT_FOUND
            )

        total_completed = self.total_price_completed_transactions(id)
        total_uncompleted = self.total_price_uncompleted_transactions(id)
        response = {
            'name': company.name,
            'total_completed_transactions': {
                'count': total_completed[1],
                'total': total_completed[0]
            },
            'total_uncompleted_transactions': {
                'count': total_uncompleted[1],
                'total': total_uncompleted[0]
            },
            'date_with_most_transactions': self.date_with_most_transactions(id)
        }

        return Response(response, status=status.HTTP_200_OK)
