""" Companies model """
from django.db import models


class Company(models.Model):
    """ Companies """
    id = models.BigAutoField(primary_key=True)

    name = models.CharField(null=False, max_length=255)
    index_name = models.CharField(null=False, max_length=255, db_index=True)

    is_active = models.BooleanField(null=False, default=False, db_index=True)
    is_deleted = models.BooleanField(default=False, db_index=True)
    deleted_at = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
