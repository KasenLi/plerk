import secrets
from datetime import datetime, timedelta
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from admin.models import User, Token


class Admin(APIView):
    """
      View for authentication to get an API Token
    """

    def post(self, request):
        """
            Validate credentials and generate an API Token
            Arguments:
            - username (string)
            - password (string)
            Return a new API Token
        """

        username = request.data.get('username', None)
        password = request.data.get('password', None)

        if username is None or password is None:
            return Response(
                {
                    'Error': 'User and password required.'
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return Response(
                {
                    'Error': 'User not found'
                },
                status=status.HTTP_404_NOT_FOUND
            )

        if user.is_valid_password(password):  # If credentials are valid
            # Generate 64 bytes API Token using secrets library
            api_token = secrets.token_urlsafe(64)
            try:
                token = Token(
                    token=api_token,
                    valid_before=datetime.now() + timedelta(minutes=60),  # Expire after 1 hour
                    user_id=user.id
                )
                token.save()
            except Exception:
                return Response(
                    {
                        'Error': 'Failed to generate token'
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )
            response = {
                'token': token.token,
                'expire_at': (datetime.now() + timedelta(minutes=60))
            }

            return Response(response, status=status.HTTP_200_OK)
        return Response(
            {
                'Error': 'Invalid username or password'
            },
            status=status.HTTP_401_UNAUTHORIZED
        )
