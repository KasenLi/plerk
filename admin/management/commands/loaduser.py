"""Load user command"""
from django.core.management.base import BaseCommand
from admin.models import User


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        """
        Load initial user
        """

        try:
            user = User.objects.get(username='admin', is_deleted=False)
            user.is_deleted = False
            user.deleted_at = None
        except User.DoesNotExist:
            user = User(
                name='Admin',
                username='admin',
                email='admin@test.com',
                password='123456'
            )

        user.save()
