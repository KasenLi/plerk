"""
User Model
"""
import bcrypt
from django.db import models


class User(models.Model):
    """
    User model
    """

    def set_password(self, password):
        """Set password"""
        self.encrypted_password = bcrypt.hashpw(
            password.encode(), bcrypt.gensalt())
        self.encrypted_password = str(self.encrypted_password.decode())

    def get_password(self):
        """Get encrypted password"""
        return self.encrypted_password

    id = models.BigAutoField(primary_key=True)

    name = models.CharField(max_length=60, null=False)

    username = models.CharField(max_length=120, null=False)
    email = models.EmailField(max_length=120, null=False)

    password = property(get_password, set_password)
    encrypted_password = models.CharField(max_length=255, null=False)

    is_deleted = models.BooleanField(default=False, db_index=True)
    deleted_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    last_ip = models.GenericIPAddressField(protocol='both', null=True)
    last_logged_in_at = models.DateTimeField(null=True)
    last_logged_out_at = models.DateTimeField(null=True)

    is_authenticated = models.BooleanField(default=False)

    def is_valid_password(self, password):
        """Validate password"""
        hashed = self.encrypted_password.encode()
        passwd = password.encode()
        return bcrypt.checkpw(passwd, hashed)

    @property
    def full_name(self):
        """Get full name"""
        return f"{self.name}"

    def __str__(self):
        """Readable property"""
        return f"User(id: {self.id}, full_name: {self.full_name})"


class Token(models.Model):
    """API Access Token"""
    id = models.BigAutoField(primary_key=True)
    token = models.CharField(max_length=100, null=False)

    last_used_ip = models.GenericIPAddressField(protocol='both', null=True)
    last_used_at = models.DateTimeField(auto_now=True, null=True)
    expired_at = models.DateTimeField(null=True)
    valid_before = models.DateTimeField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=12, choices=(
        ('EXPIRED', 'Expired'),
        ('DISABLED', 'Disabled'),
        ('OWNERUPDATED', 'Owner Updated'),
        ('AVAILABLE', 'Available'),
        ('OWNERDELETED', 'Owner Deleted')
    ), default='AVAILABLE')
    user = models.ForeignKey('admin.User', on_delete=models.CASCADE, null=True,
                             related_name='tokens', default=None)
